package com.sunbeaminfo.sh.hb.daos;

import org.hibernate.Session;

import com.sunbeaminfo.sh.hb.entities.Dept;
import com.sunbeaminfo.sh.hb.entities.Emp;
import com.sunbeaminfo.sh.hb.entities.Meeting;
import com.sunbeaminfo.sh.hb.utils.HbUtil;

public class EmpDeptDao {
	public Emp getEmp(int empno) {
		Session session = HbUtil.getSession();
		return session.get(Emp.class, empno);
	}
	
	public Dept getDept(int deptno) {
		Session session = HbUtil.getSession();
		return session.get(Dept.class, deptno);
	}

	public void addDept(Dept d) {
		Session session = HbUtil.getSession();
		session.persist(d);
	}
	
	public void deleteDept(int id) {
		Session session = HbUtil.getSession();
		Dept d = getDept(id);
		session.remove(d);
	}

	public Meeting getMeeting(int meetingId) {
		Session session = HbUtil.getSession();
		return session.get(Meeting.class, meetingId);
	}

	public void addMeeting(Meeting m) {
		Session session = HbUtil.getSession();
		session.persist(m);
	}
}
