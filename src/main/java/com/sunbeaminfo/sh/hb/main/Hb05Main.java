package com.sunbeaminfo.sh.hb.main;

import java.util.Date;

import com.sunbeaminfo.sh.hb.daos.EmpDeptDao;
import com.sunbeaminfo.sh.hb.entities.Address;
import com.sunbeaminfo.sh.hb.entities.Dept;
import com.sunbeaminfo.sh.hb.entities.Emp;
import com.sunbeaminfo.sh.hb.entities.Meeting;
import com.sunbeaminfo.sh.hb.utils.HbUtil;

public class Hb05Main {
	public static void main(String[] args) {
		EmpDeptDao dao = new EmpDeptDao();
		
//		try {
//			HbUtil.beginTransaction();
//			Emp e = dao.getEmp(7900);
//			System.out.println("Found : " + e);
//			Dept d = e.getDept();
//			System.out.println("Emp's Dept : " + d);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}
		
//		try {
//			HbUtil.beginTransaction();
//			Dept d = dao.getDept(10);
//			System.out.println("Found : " + d);
//			for (Emp e : d.getEmpList())
//				System.out.println(e);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}

//		try {
//			HbUtil.beginTransaction();
//			Dept d = new Dept(50, "TRAINING", "PUNE");
//			d.getEmpList().add(new Emp(1, "Nilesh", "Trainer", 0, null, 20000.00, null, d));
//			d.getEmpList().add(new Emp(2, "Sandeep", "Trainer", 1, null, 23000.00, null, d));
//			d.getEmpList().add(new Emp(3, "Smita", "Trainer", 1, null, 23000.00, null, d));
//			dao.addDept(d);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}

//		try {
//			HbUtil.beginTransaction();
//			dao.deleteDept(50);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}

		
//		try {
//			HbUtil.beginTransaction();
//			Emp e = dao.getEmp(7900);
//			System.out.println(e);
//			Address a = e.getAddr();
//			System.out.println(a);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}

//		try {
//			HbUtil.beginTransaction();
//			Emp e = dao.getEmp(7934);
//			System.out.println(e);
//			for(Meeting m : e.getMeetings())
//				System.out.println(m);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}

//		try {
//			HbUtil.beginTransaction();
//			Meeting m = dao.getMeeting(2);
//			System.out.println(m);
//			for(Emp e : m.getEmpList())
//				System.out.println(e);
//			HbUtil.commitTransaction();
//		} catch (Exception e) {
//			e.printStackTrace();
//			HbUtil.rollbackTransaction();
//		}

		
		try {
			HbUtil.beginTransaction();
			Meeting m = new Meeting(3, "Client", new Date());
			m.getEmpList().add(dao.getEmp(7844));
			m.getEmpList().add(dao.getEmp(7934));
			m.getEmpList().add(dao.getEmp(7654));
			for (Emp e : m.getEmpList())
				e.getMeetings().add(m);
			dao.addMeeting(m);
			HbUtil.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			HbUtil.rollbackTransaction();
		}
		
		HbUtil.shutdown();
	}
}
