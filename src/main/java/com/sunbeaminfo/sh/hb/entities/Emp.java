package com.sunbeaminfo.sh.hb.entities;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "EMP")
public class Emp {
	@Id
	private int empno;
	@Column
	private String ename;
	@Column
	private String job;
	@Column
	private Integer mgr;
	@Column
	private Date hire;
	@Column
	private Double sal;
	@Column
	private Double comm;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deptno") // foreign key column of Emp table.
	private Dept dept;
	@OneToOne
	@PrimaryKeyJoinColumn
	private Address addr;
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="EMPMEETING", 
		joinColumns= { @JoinColumn(name="empid")}, 
		inverseJoinColumns= { @JoinColumn(name="meetingid") })
	private List<Meeting> meetings;

	public Emp() {
		this(0, "", "", 0, new Date(), 0.0, 0.0);
	}

	public Emp(int empno, String ename, String job, Integer mgr, Date hire, Double sal, Double comm) {
		this(0, "", "", 0, new Date(), 0.0, 0.0, new Dept());
	}

	public Emp(int empno, String ename, String job, Integer mgr, Date hire, Double sal, Double comm, Dept dept) {
		this.empno = empno;
		this.ename = ename;
		this.job = job;
		this.mgr = mgr;
		this.hire = hire;
		this.sal = sal;
		this.comm = comm;
		this.dept = dept;
		this.meetings = new ArrayList<Meeting>();
	}

	public List<Meeting> getMeetings() {
		return meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}

	public Address getAddr() {
		return addr;
	}

	public void setAddr(Address addr) {
		this.addr = addr;
	}

	public int getEmpno() {
		return empno;
	}

	public void setEmpno(int empno) {
		this.empno = empno;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Integer getMgr() {
		return mgr;
	}

	public void setMgr(Integer mgr) {
		this.mgr = mgr;
	}

	public Date getHire() {
		return hire;
	}

	public void setHire(Date hire) {
		this.hire = hire;
	}

	public Double getSal() {
		return sal;
	}

	public void setSal(Double sal) {
		this.sal = sal;
	}

	public Double getComm() {
		return comm;
	}

	public void setComm(Double comm) {
		this.comm = comm;
	}
	
	

	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

	@Override
	public String toString() {
		return "Emp [empno=" + empno + ", ename=" + ename + ", job=" + job + ", mgr=" + mgr + ", hire=" + hire
				+ ", sal=" + sal + ", comm=" + comm + "]";
	}
}






